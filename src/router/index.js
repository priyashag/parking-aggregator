import Vue from 'vue'
import Router from 'vue-router'
import ParkOz from '@/pages/ParkOz'
import UserLocation from '@/pages/UserLocation'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/ParkOz',
      component: ParkOz
    },
    { 
      path: '/',
      component: UserLocation
    }
  ]
})
